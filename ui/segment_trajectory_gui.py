import sys
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg


class TrajectoryWidget:

    def __init__(self):
        self.params = {}
    
    def launch(self, data, existing={}, w_type='w_filt'):        
        app = QtGui.QApplication([])
        mw = QtGui.QMainWindow()
        mw.setWindowTitle("Task Phase Segmentation")
        mw.resize(1500, 1000)
        cw = QtGui.QWidget()
        layout = QtGui.QGridLayout()
        win_left = pg.GraphicsLayoutWidget()

        mw.setCentralWidget(cw)
        cw.setLayout(layout)
        layout.addWidget(win_left, 0, 0)

        win_right = pg.LayoutWidget()
        save_btn = QtGui.QPushButton("Save")
        exit_btn = QtGui.QPushButton("Exit Without Saving")
        win_right.addWidget(save_btn)
        win_right.nextRow()
        win_right.addWidget(exit_btn)
        layout.addWidget(win_right, 0, 1)
        
        pg.setConfigOptions(antialias=True)
        self.num_pts = data['x'].shape[1]
        xs = np.linspace(0.0, self.num_pts, self.num_pts)

        p1 = win_left.addPlot(title="Position X")
        p1.plot(data['x'][0,:], pen=(255,255,255,200))
        f1 = win_left.addPlot(title="Force X")
        f1.plot(data[w_type][0,:], pen=(255,255,255,200))
        
        win_left.nextRow()

        p2 = win_left.addPlot(title="Position Y")
        p2.plot(data['x'][1,:], pen=(255,255,255,200))
        f2 = win_left.addPlot(title="Force Y")
        f2.plot(data[w_type][1,:], pen=(255,255,255,200))

        win_left.nextRow()

        p3 = win_left.addPlot(title="Position Z")
        p3.plot(data['x'][2,:], pen=(255,255,255,200))
        f3 = win_left.addPlot(title="Force Z")
        f3.plot(data[w_type][2,:], pen=(255,255,255,200))

        p1_r = pg.LinearRegionItem([400,700])
        p2_r = pg.LinearRegionItem([400,700])
        p3_r = pg.LinearRegionItem([400,700])
        f1_r = pg.LinearRegionItem([400,700])
        f2_r = pg.LinearRegionItem([400,700])
        f3_r = pg.LinearRegionItem([400,700])

        p1_sl = pg.InfiniteLine(angle=90, movable=True)
        p2_sl = pg.InfiniteLine(angle=90, movable=True)
        p3_sl = pg.InfiniteLine(angle=90, movable=True)
        f1_sl = pg.InfiniteLine(angle=90, movable=True)
        f2_sl = pg.InfiniteLine(angle=90, movable=True)
        f3_sl = pg.InfiniteLine(angle=90, movable=True)

        p1_fl = pg.InfiniteLine(angle=90, movable=True)
        p2_fl = pg.InfiniteLine(angle=90, movable=True)
        p3_fl = pg.InfiniteLine(angle=90, movable=True)
        f1_fl = pg.InfiniteLine(angle=90, movable=True)
        f2_fl = pg.InfiniteLine(angle=90, movable=True)
        f3_fl = pg.InfiniteLine(angle=90, movable=True)

        p1.addItem(p1_r)
        p2.addItem(p2_r)
        p3.addItem(p3_r)
        f1.addItem(f1_r)
        f2.addItem(f2_r)
        f3.addItem(f3_r)

        p1.addItem(p1_sl)
        p2.addItem(p2_sl)
        p3.addItem(p3_sl)
        f1.addItem(f1_sl)
        f2.addItem(f2_sl)
        f3.addItem(f3_sl)

        p1.addItem(p1_fl)
        p2.addItem(p2_fl)
        p3.addItem(p3_fl)
        f1.addItem(f1_fl)
        f2.addItem(f2_fl)
        f3.addItem(f3_fl)

        def update_regions_p1():
            region = p1_r.getRegion()
            p2_r.setRegion(region)
            p3_r.setRegion(region)
            f1_r.setRegion(region)
            f2_r.setRegion(region)
            f3_r.setRegion(region)  
            p1_sl.setBounds([0, region[0]])
            p2_sl.setBounds([0, region[0]])
            p3_sl.setBounds([0, region[0]])
            f1_sl.setBounds([0, region[0]])
            f2_sl.setBounds([0, region[0]])
            f3_sl.setBounds([0, region[0]])
            p1_fl.setBounds([region[1], self.num_pts])
            p2_fl.setBounds([region[1], self.num_pts])
            p3_fl.setBounds([region[1], self.num_pts])
            f1_fl.setBounds([region[1], self.num_pts])
            f2_fl.setBounds([region[1], self.num_pts])
            f3_fl.setBounds([region[1], self.num_pts])

        def update_regions_p2():
            region = p2_r.getRegion()
            p1_r.setRegion(region)

        def update_regions_p3():
            region = p3_r.getRegion()
            p1_r.setRegion(region)

        def update_regions_f1():
            region = f1_r.getRegion()
            p1_r.setRegion(region)

        def update_regions_f2():
            region = f2_r.getRegion()
            p1_r.setRegion(region)

        def update_regions_f3():
            region = f3_r.getRegion()
            p1_r.setRegion(region)

        def update_sl_p1():
            value = p1_sl.value()
            p2_sl.setValue(value)
            p3_sl.setValue(value)
            f1_sl.setValue(value)
            f2_sl.setValue(value)
            f3_sl.setValue(value)
            
        def update_sl_p2():
            value = p2_sl.value()
            p1_sl.setValue(value)

        def update_sl_p3():
            value = p3_sl.value()
            p1_sl.setValue(value)

        def update_sl_f1():
            value = f1_sl.value()
            p1_sl.setValue(value)

        def update_sl_f2():
            value = f2_sl.value()
            p1_sl.setValue(value)

        def update_sl_f3():
            value = f3_sl.value()
            p1_sl.setValue(value)
            
        def update_fl_p1():
            value = p1_fl.value()
            p2_fl.setValue(value)
            p3_fl.setValue(value)
            f1_fl.setValue(value)
            f2_fl.setValue(value)
            f3_fl.setValue(value)
            
        def update_fl_p2():
            value = p2_fl.value()
            p1_fl.setValue(value)

        def update_fl_p3():
            value = p3_fl.value()
            p1_fl.setValue(value)

        def update_fl_f1():
            value = f1_fl.value()
            p1_fl.setValue(value)

        def update_fl_f2():
            value = f2_fl.value()
            p1_fl.setValue(value)

        def update_fl_f3():
            value = f3_fl.value()
            p1_fl.setValue(value)

        def exit():
            #app.closeAllWindows()
            sys.exit()

        def save_params():
            print "Saving selected parameters."
            # TODO should be able to accommodate arbitrary phases
            self.params['phase_1'] = {}
            self.params['phase_2'] = {}
            self.params['phase_3'] = {}

            self.params['phase_1']['start'] = p1_sl.value()
            self.params['phase_1']['end'] = p1_r.getRegion()[0]
            self.params['phase_2']['start'] = p1_r.getRegion()[0]
            self.params['phase_2']['end'] = p1_r.getRegion()[1]
            self.params['phase_3']['start'] = p1_r.getRegion()[1]
            self.params['phase_3']['end'] = p1_fl.value()
            
        p1_r.sigRegionChanged.connect(update_regions_p1)
        p2_r.sigRegionChanged.connect(update_regions_p2)
        p3_r.sigRegionChanged.connect(update_regions_p3)
        f1_r.sigRegionChanged.connect(update_regions_f1)
        f2_r.sigRegionChanged.connect(update_regions_f2)
        f3_r.sigRegionChanged.connect(update_regions_f3)

        p1_sl.sigPositionChanged.connect(update_sl_p1)
        p2_sl.sigPositionChanged.connect(update_sl_p2)
        p3_sl.sigPositionChanged.connect(update_sl_p3)
        f1_sl.sigPositionChanged.connect(update_sl_f1)
        f2_sl.sigPositionChanged.connect(update_sl_f2)
        f3_sl.sigPositionChanged.connect(update_sl_f3)

        p1_fl.sigPositionChanged.connect(update_fl_p1)
        p2_fl.sigPositionChanged.connect(update_fl_p2)
        p3_fl.sigPositionChanged.connect(update_fl_p3)
        f1_fl.sigPositionChanged.connect(update_fl_f1)
        f2_fl.sigPositionChanged.connect(update_fl_f2)
        f3_fl.sigPositionChanged.connect(update_fl_f3)

        # set positions to existing if provided, otherwise generically spaced
        if existing:
            p1_sl.setValue(existing['start'])
            p1_fl.setValue(existing['end'])
            p1_r.setRegion([existing['c_start'], existing['c_end']])
        else:
            p1_sl.setValue(0)            
            p1_fl.setValue(self.num_pts)
            p1_r.setRegion([int(self.num_pts / 3.0), int(2.0 * self.num_pts / 3.0)])
        
        save_btn.clicked.connect(save_params)
        exit_btn.clicked.connect(exit)
        
        update_regions_p1()

        mw.show()
        
        #if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
        return self.params

    
if __name__ == '__main__':
    tw = TrajectoryWidget()
    data = {}
    data['x'] = np.ones(6000).reshape((6, 1000))
    w_type='w_filt'
    data[w_type] = np.ones(6000).reshape((6, 1000))
    params = tw.launch(data)
    print params
    
