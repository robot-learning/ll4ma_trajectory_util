#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from rospy_service_helper import get_task_trajectories, get_joint_trajectories
from ll4ma_trajectory_util.srv import GenerateJointTrajectory, GenerateJointTrajectoryRequest
from ll4ma_trajectory_util.srv import VisualizeJointTrajectory, VisualizeJointTrajectoryRequest

rospy.init_node("test_trajectory_generator")
rospy.loginfo("Waiting for data service...")
rospy.wait_for_service("/pandas/get_task_trajectories")
rospy.loginfo("Found!")

t_traj = get_task_trajectories("/pandas/get_task_trajectories", "end_effector", ["trajectory_006"])[0]
j_traj = get_joint_trajectories("/pandas/get_joint_trajectories", "lbr4", ["trajectory_006"])[0]

req = GenerateJointTrajectoryRequest()
req.task_trajectory = t_traj
req.init_joint_state = JointState(position=j_traj.points[0].positions)

rospy.loginfo("Trying to generate trajectory...")
success = False
try:
    gen = rospy.ServiceProxy("/trajectory_util/generate_joint_trajectory", GenerateJointTrajectory)
    resp = gen(req)
    computed_j_traj = resp.joint_trajectory
    success = resp.success
except rospy.ServiceException as e:
    rospy.logerr("OOOPS: {}".format(e))

if success:
    rospy.loginfo("Trajectory successfully generated.")
    rospy.loginfo("Trying to visualize trajectory...")
    viz_req = VisualizeJointTrajectoryRequest()
    viz_req.joint_trajectory = computed_j_traj
    viz_req.joint_trajectory.joint_names = ["lbr4_j{}".format(joint) for joint in range(7)]
    viz_req.joint_trajectory.header.frame_id = "base_link"
    try:
        viz = rospy.ServiceProxy("/visualization/visualize_robot_trajectory", VisualizeJointTrajectory)
        viz(viz_req)
        rospy.loginfo("Trajectory successfully visualized")
    except rospy.ServiceException as e:
        rospy.logwarn("OOPS: {}".format(e))
else:
    rospy.logwarn("WHAT THE FUCK")

