#!/usr/bin/env python
import os
import rospy
import numpy as np
import tf.transformations as tf
from ll4ma_trajectory_util.srv import (VisualizeTrajectories,
                                       VisualizeTrajectoriesRequest)
from std_srvs.srv import Trigger
from geometry_msgs.msg import Pose, Quaternion


def visualize_trajectories(trajectory_names=[], color_name="dodgerblue"):
    req = VisualizeTrajectoriesRequest()
    req.trajectory_names = trajectory_names
    req.color_name = color_name
    try:
        visualize = rospy.ServiceProxy("visualization/visualize_trajectories",
                                       VisualizeTrajectories)
        resp = visualize(req)
        success = resp.success
    except rospy.ServiceException as e:
        rospy.logwarn("Service request to visualize trajectory failed: %s" % e)
        return False
    if not success:
        rospy.logwarn("Could not visualize trajectory.")
        return False
    else:
        return True


def parse_args(args):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--abs_path',
        dest="abs_path",
        default="/media/adam/data_haro/rss_2019")
    parser.add_argument(
        '--rel_path',
        dest="rel_path",
        default="experiment_1__grid_data/trajectory_data")
    parsed_args = parser.parse_args(sys.argv[1:])
    return parsed_args


if __name__ == '__main__':
    rospy.init_node("visualize_trajectory_session")
    import sys

    rospy.loginfo("Waiting for services...")
    rospy.wait_for_service("visualization/visualize_trajectories")
    rospy.loginfo("Services are up!")

    args = parse_args(sys.argv[1:])

    data_path = os.path.join(args.abs_path, args.rel_path)
    trajectory_names = []
    for filename in os.listdir(data_path):
        if not filename.endswith(".pkl"):
            continue
        trajectory_name, _ = filename.split(".")
        # Skipping these they same bad:
        if trajectory_name in [
                "trajectory_023", "trajectory_178", "trajectory_179"
        ]:
            continue
        trajectory_names.append(trajectory_name)

    visualize_trajectories(trajectory_names, "darkblue")
