#ifndef JOINT_TRAJECTORY_GENERATOR_H
#define JOINT_TRAJECTORY_GENERATOR_H

// ROS
#include <ros/ros.h>
#include <ll4ma_trajectory_util/GenerateJointTrajectory.h>

// KDL
#include <ll4ma_kdl/manipulator_kdl/robot_kdl.h>

// boost
#include <boost/scoped_ptr.hpp>

// Eigen
#include <Eigen/Dense>


class JointTrajectoryGenerator
{
private:

  int num_jnts_;
  std::string root_link_, tip_link_, robot_description_;
  std::vector<std::string> jnt_names_, tip_links_, root_links_;
  std::vector<double> jnt_upper_lims_, jnt_lower_lims_;
  std::vector<double> gravity_ = {0.0,0.0,-9.8};
  
  // KDL
  boost::shared_ptr<manipulator_kdl::robotKDL> kdl_;
  KDL::Frame x_kdl_, prev_x_kdl_;
  KDL::Twist x_diff_kdl_;

  // Eigen
  Eigen::VectorXd q_, q_diff_, x_diff_;
  Eigen::MatrixXd J_, J_inv_;

  // ROS
  ros::NodeHandle nh_;
  ros::Rate rate_;
  ros::Duration duration_;
  ros::ServiceServer gen_traj_srv_;

  void getPseudoInverse(Eigen::MatrixXd &m, Eigen::MatrixXd &m_pinv, double tolerance);
  bool generateJointTrajectory(ll4ma_trajectory_util::GenerateJointTrajectory::Request  &req,
                               ll4ma_trajectory_util::GenerateJointTrajectory::Response &resp);
  void poseMsgToKDL(const geometry_msgs::Pose &m, KDL::Frame &k);

public:
  JointTrajectoryGenerator() : rate_(100), nh_("~") {}
  bool init();
  void run();
};


#endif
