#include "trajectory_generation/joint_trajectory_generator.h"
#include <trajectory_msgs/JointTrajectoryPoint.h>

bool JointTrajectoryGenerator::init()
{
  ROS_INFO("Initializing JointTrajectoryGenerator...");

  // nh_.reset(&nh);

  // Load ROS parameter values from server
  bool success = true;
  success &= nh_.getParam("tip_links", tip_links_);
  success &= nh_.getParam("root_links", root_links_);
  success &= nh_.getParam("joint_upper_limits", jnt_upper_lims_);
  success &= nh_.getParam("joint_lower_limits", jnt_lower_lims_);
  if (!success)
  {
    ROS_ERROR("Failed to load params from param server.");
    return false;
  }

  // Initialize KDL
  kdl_.reset(new manipulator_kdl::robotKDL("/robot_description", nh_, root_links_,
                                           tip_links_, gravity_));
  jnt_names_ = kdl_->getJointNames();
  num_jnts_ = jnt_names_.size();

  if (jnt_upper_lims_.size() != num_jnts_ || jnt_lower_lims_.size() != num_jnts_)
  {
    ROS_ERROR_STREAM("Number of joints for limits not as expected: Upper ("
                     << jnt_upper_lims_.size() << "), Lower (" << jnt_lower_lims_.size()
                     << "), Expected (" << num_jnts_ << ")");
    return false;
  }

  // Initialize Eigen
  x_diff_.setZero(6);
  q_.setZero(num_jnts_);
  q_diff_.setZero(num_jnts_);
  J_.setZero(6, num_jnts_);
  J_inv_.setZero(num_jnts_, 6);

  // Services this node offers
  gen_traj_srv_ = nh_.advertiseService("/trajectory_util/generate_joint_trajectory",
                                       &JointTrajectoryGenerator::generateJointTrajectory, this);

  ROS_INFO("Initialization of JointTrajectoryGenerator complete.");
  return true;
}

void JointTrajectoryGenerator::run()
{
  ROS_INFO("Ready to generate joint trajectories.");
  while (ros::ok())
  {
    ros::spinOnce();
    rate_.sleep();
  }
  ROS_INFO("Exiting.");
}

bool JointTrajectoryGenerator::generateJointTrajectory(
    ll4ma_trajectory_util::GenerateJointTrajectory::Request &req,
    ll4ma_trajectory_util::GenerateJointTrajectory::Response &resp)
{
  ROS_INFO("Generating joint trajectory...");

  if (req.init_joint_state.position.size() == 0)
  {
    ROS_ERROR("Must provide initial joint state to generate a joint trajectory.");
    return false;
  }
  if (req.init_joint_state.position.size() != num_jnts_)
  {
    ROS_ERROR_STREAM("Size of joint state (" << req.init_joint_state.position.size() << ") does not match expected number of joints (" << num_jnts_ << ").");
    return false;
  }

  // Get initial joint angles
  for (int i = 0; i < req.init_joint_state.position.size(); ++i)
    q_[i] = req.init_joint_state.position[i];

  duration_ = ros::Duration(0.0);

  bool first_point = true;
  for (auto const &t_point : req.task_trajectory.points)
  {
    // Get the current pose
    poseMsgToKDL(t_point.pose, x_kdl_);
    if (first_point)
    {
      prev_x_kdl_ = x_kdl_;
      first_point = false;
    }

    // Compute the pose difference from last point
    x_diff_kdl_ = KDL::diff(prev_x_kdl_, x_kdl_); // current - previous
    for (int i = 0; i < 6; ++i)
      x_diff_[i] = x_diff_kdl_[i];

    // Compute joint angle difference from pose difference using inverse Jacobian mapping
    kdl_->getJacobian(0, q_, J_);
    getPseudoInverse(J_, J_inv_, 1.e-5);
    q_diff_ = J_inv_ * x_diff_;

    // Compute joint angles and save to the running joint trajectory
    q_ += q_diff_;

    trajectory_msgs::JointTrajectoryPoint q_point;
    for (int i = 0; i < q_.size(); ++i)
    {
      // Cut off at limits
      q_(i) = std::max(q_(i), jnt_lower_lims_[i]);
      q_(i) = std::min(q_(i), jnt_upper_lims_[i]);
      q_point.positions.push_back(q_[i]);
      q_point.velocities.push_back(q_diff_[i]); //  / 0.01);
      // q_point.velocities.push_back(0.0);
    }
    q_point.time_from_start = duration_;

    // TODO want to do null space projection

    resp.joint_trajectory.points.push_back(q_point);
    prev_x_kdl_ = x_kdl_;
    duration_ += ros::Duration(0.01);
  }

  ROS_INFO("Joint trajectory generated successfully.");
  resp.success = true;
  return true;
}

void JointTrajectoryGenerator::getPseudoInverse(Eigen::MatrixXd &m, Eigen::MatrixXd &m_pinv,
                                                double tolerance)
{
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(m, Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType sing_vals = svd.singularValues();
  // set values within tolerance to zero
  for (int idx = 0; idx < sing_vals.size(); idx++)
  {
    if (tolerance > 0.0 && sing_vals(idx) > tolerance)
      sing_vals(idx) = 1.0 / sing_vals(idx);
    else
      sing_vals(idx) = 0.0;
  }

  m_pinv = svd.matrixV().leftCols(sing_vals.size()) * sing_vals.asDiagonal() * svd.matrixU().leftCols(sing_vals.size()).transpose();
}

// This function is from kdl_msg.cpp in the kdl_conversions library of geometry package
void JointTrajectoryGenerator::poseMsgToKDL(const geometry_msgs::Pose &m, KDL::Frame &k)
{
  k.p[0] = m.position.x;
  k.p[1] = m.position.y;
  k.p[2] = m.position.z;

  k.M = KDL::Rotation::Quaternion(m.orientation.x, m.orientation.y, m.orientation.z, m.orientation.w);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "joint_trajectory_generator");
  // ros::NodeHandle nh("~");
  JointTrajectoryGenerator generator;
  if (generator.init())
    generator.run();
  else
    ROS_ERROR("Bad things happened in initializing JointTrajectoryGenerator");
}
