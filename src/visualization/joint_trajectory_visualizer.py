#!/usr/bin/env python
import rospy
from moveit_msgs.msg import DisplayTrajectory, RobotTrajectory
from ll4ma_trajectory_util.srv import VisualizeJointTrajectory, VisualizeJointTrajectoryResponse
from trajectory_msgs.msg import JointTrajectoryPoint


class TrajectoryVisualizer:

    def __init__(self):
        display_topic = rospy.get_param("~display_topic", "/display_robot_trajectory")
        rate          = rospy.get_param("~rate", 100.0)
        self.display_pub = rospy.Publisher(display_topic, DisplayTrajectory, queue_size=1)
        self.rate        = rospy.Rate(rate)
        self.viz_srv     = rospy.Service("/visualization/visualize_robot_trajectory",
                                         VisualizeJointTrajectory, self._visualize_joint_trajectory)

    def run(self):
        rospy.loginfo("Ready to visualize trajectories.")
        while not rospy.is_shutdown():
            self.rate.sleep()

    def _visualize_joint_trajectory(self, req):
        robot_traj = RobotTrajectory(joint_trajectory=req.joint_trajectory)
        self.display = DisplayTrajectory()
        self.display.model_id = "lbr4"
        self.display.trajectory.append(robot_traj)
        self.display_pub.publish(self.display)
        return VisualizeJointTrajectoryResponse(success=True)
        

if __name__ == '__main__':
    rospy.init_node('trajectory_visualizer')
    visualizer = TrajectoryVisualizer()
    try:
        visualizer.run()
    except rospy.ROSInterruptException:
        pass
