#!/usr/bin/env python
import rospy
import numpy as np
import tf.transformations as tf
from matplotlib import colors
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Point, Pose, PoseArray, Quaternion
from sensor_msgs.msg import JointState
from visualization_msgs.msg import Marker, MarkerArray
from ll4ma_trajectory_util.srv import (
    GetTaskTrajectories,
    GetTaskTrajectoriesRequest,
    GetJointTrajectories,
    GetJointTrajectoriesRequest,
    VisualizeTrajectories,
    VisualizeTrajectoriesResponse,
)
from std_srvs.srv import Trigger, TriggerResponse
from ll4ma_trajectory_msgs.msg import TaskTrajectory, TaskTrajectoryPoint
from rospy_service_helper import SRV_NAMES, get_task_trajectories


class MultiTrajectoryVisualizer:
    def __init__(self):
        self.base_frame = rospy.get_param("~base_frame")
        marker_topic = rospy.get_param("~marker_topic", "trajectory_viz")
        self.rate = rospy.Rate(1)
        self.marker_pub = rospy.Publisher(
            marker_topic, MarkerArray, queue_size=1)
        self.marker_idx = 0
        self.marker_array = MarkerArray()

        # Services this node offers
        self.viz_traj_srv = rospy.Service(
            "/visualization/visualize_trajectories", VisualizeTrajectories,
            self.visualize_trajs)
        self.clear_traj_srv = rospy.Service(
            "/visualization/clear_trajectories", Trigger, self.clear_trajs)

        rospy.loginfo("Waiting for services...")
        rospy.wait_for_service("/pandas/get_task_trajectories")
        rospy.loginfo("Services are up!")

    def run(self):
        rospy.loginfo("Trajectory visualization services are available.")
        while not rospy.is_shutdown():
            if self.marker_array.markers:
                self.marker_pub.publish(self.marker_array)
            self.rate.sleep()

    # === BEGIN service functions being offered ===============================

    def visualize_trajs(self, req):
        rospy.loginfo("Generating trajectory visualization...")
        resp = VisualizeTrajectoriesResponse()
        resp.success = self._visualize_trajs(req.trajectory_names,
                                             req.color_name)
        rospy.loginfo("Visualization complete.")
        return resp

    def clear_trajs(self, req):
        self.marker_array.markers = []
        resp = TriggerResponse()
        resp.success = not self.marker_array.markers
        return resp

    # === END service functions being offered =================================

    def _visualize_trajs(self, trajectory_names=[], color_name=None):
        if not color_name:
            color_name = "dodgerblue"

        trajs = get_task_trajectories(SRV_NAMES["get_task_trajs"],
                                      "end_effector_pose_base_frame",
                                      trajectory_names)

        # Populate a marker and add it to the marker array
        for traj in trajs:
            marker = self._get_marker(traj, color_name)
            self.marker_array.markers.append(marker)

        return True

    def _get_marker(self, traj, color_name, size=0.001):
        m = Marker()
        m.header.frame_id = self.base_frame
        m.header.stamp = rospy.Time.now()
        m.id = self.marker_idx
        m.type = Marker.LINE_STRIP
        m.action = Marker.ADD
        m.scale.x = size
        m.color = self._get_color(color_name)
        for point in traj.points:
            position = point.pose.position
            m.points.append(Point(position.x, position.y, position.z))
        self.marker_idx += 1
        return m

    def _get_color(self, color_name):
        """
        color can be any name from this page:
        http://matplotlib.org/mpl_examples/color/named_colors.hires.png
        """
        converter = colors.ColorConverter()
        c = converter.to_rgba(colors.cnames[color_name])
        return ColorRGBA(*c)


if __name__ == '__main__':
    rospy.init_node('multi_trajectory_visualizer')
    visualizer = MultiTrajectoryVisualizer()
    try:
        visualizer.run()
    except rospy.ROSInterruptException:
        pass
