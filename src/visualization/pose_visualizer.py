#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseArray
from ll4ma_trajectory_util.srv import VisualizePoses


class PoseVisualizer:
    def __init__(self):
        self.pose_array = PoseArray()
        self.pose_pub = rospy.Publisher(
            "/object_poses", PoseArray, queue_size=1)
        self.viz_pose_srv = rospy.Service("/visualization/visualize_poses",
                                          VisualizePoses,
                                          self._visualize_poses)
        self.rate = rospy.Rate(100)

    def run(self):
        rospy.loginfo("Ready to visualize poses...")
        while not rospy.is_shutdown():
            self.rate.sleep()

    def _visualize_poses(self, req):
        self.pose_array.poses = req.poses
        self.pose_array.header.frame_id = req.base_link
        self.pose_array.header.stamp = rospy.Time.now()
        self.pose_pub.publish(self.pose_array)
        return True


if __name__ == '__main__':
    rospy.init_node('pose_visualizer')
    visualizer = PoseVisualizer()
    visualizer.run()