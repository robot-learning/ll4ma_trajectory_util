#!/usr/bin/env python
import sys
import rospy
from geometry_msgs.msg import PoseStamped
from ll4ma_robot_control.msg import RobotState


class ConstraintFrameVisualizer:

    def __init__(self, base_frame='lbr4_0_link', rate=100):
        self.base_frame = base_frame
        self.rate = rospy.Rate(float(rate))
        self.robot_name = rospy.get_param("/robot_name")
        self.robot_state = None
        self.pose_stmp = PoseStamped()
        self.pose_stmp.header.frame_id = self.base_frame
        pose_topic = "/%s/constraint_frame/pose" % self.robot_name
        robot_state_topic = "/%s/robot_state" % self.robot_name
        self.pub = rospy.Publisher(pose_topic, PoseStamped, queue_size=1)
        rospy.Subscriber(robot_state_topic, RobotState, self._robot_state_cb)

    def publish(self):
        self.pose_stmp.pose = self.robot_state.constraint_frame
        self.pose_stmp.header.stamp = rospy.Time.now()
        self.pub.publish(self.pose_stmp)
        
    def run(self):
        rospy.loginfo("[ConstraintFrameVisualizer] Waiting for robot state.")
        while not rospy.is_shutdown() and self.robot_state is None:
            self.rate.sleep()
        rospy.loginfo("[ConstraintFrameVisualizer] Robot state received.")
        rospy.loginfo("[ConstraintFrameVisualizer] Publishing constraint frame pose...")
        while not rospy.is_shutdown():
            self.publish()

    def shutdown(self):
        rospy.loginfo("[ConstraintFrameVisualizer] Exiting.")
        
    def _robot_state_cb(self, robot_state):
        self.robot_state = robot_state

    
if __name__ == '__main__':
    rospy.init_node('constraint_frame_visualizer')
    cfv = ConstraintFrameVisualizer()
    rospy.on_shutdown(cfv.shutdown)
    cfv.run()
