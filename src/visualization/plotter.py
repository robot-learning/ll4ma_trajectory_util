import numpy as np
import matplotlib.pyplot as plt


class Plotter:

    def __init__(self):
        pass
    
    def plot_joint_trajectory(self, data, ylabels=[]):
        fig = plt.figure(figsize=(23,5))
        fig.subplots_adjust(wspace=.25)
        plt.suptitle('Joint Position', fontweight='bold', fontsize=18)
        num_dims, num_pts = data.shape
        t = np.arange(num_pts)
        for i in range(num_dims):
            plt.subplot(171 + i)
            plt.plot(t, data[i,:])
            if ylabels:
                plt.ylabel(ylabels[i], fontsize=16)
        plt.tight_layout()
        plt.show()


def test():
    plotter = Plotter()
    data = np.ones((7,100))
    plotter.plot_joint_trajectory(data)


if __name__ == '__main__':
    test()
