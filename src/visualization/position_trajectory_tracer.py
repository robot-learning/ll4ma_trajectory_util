#!/usr/bin/env python
import sys
import rospy
from matplotlib import colors
from geometry_msgs.msg import Point, PoseStamped
from visualization_msgs.msg import Marker


class PositionTrajectoryTracer:

    def __init__(self, pose_topic, marker_topic='visualization_marker', style='point',
                 size=0.01, color='darkgreen', base_frame='lbr4_0_link', rate=100):
        self.pose_topic   = rospy.get_param("~pose_topic", "/pose")
        self.marker_topic = rospy.get_param("~marker_topic", "/position_trajectory_trace")
        self.style        = rospy.get_param("~style", "point")
        self.size         = rospy.get_param("~size", 0.01)
        self.color        = rospy.get_param("~color", "red")
        self.base_frame   = rospy.get_param("~base_frame", "lbr4_base_link")
        self.rate_val     = rospy.get_param("~rate", 100.0)
                                          
        self.marker     = self._get_marker(self.style, float(self.size), self.color)
        self.pose       = None
        self.rate       = rospy.Rate(self.rate_val)
        self.marker_pub = rospy.Publisher(self.marker_topic, Marker, queue_size=1)
        rospy.Subscriber(self.pose_topic, PoseStamped, self._pose_cb)

    def publish_marker(self):
        self.marker.points.append(Point(self.pose.position.x,
                                        self.pose.position.y,
                                        self.pose.position.z))
        self.marker_pub.publish(self.marker)

    def run(self):
        if self.marker is None:
            rospy.logerr("Marker could not be created.")
            return
        rospy.loginfo("Waiting for pose...")
        while not rospy.is_shutdown() and self.pose is None:
            self.rate.sleep()
        rospy.loginfo("Pose received.")
        rospy.loginfo("Publishing markers...")
        while not rospy.is_shutdown():
            self.publish_marker()
            self.rate.sleep()
        rospy.loginfo("Complete. Exiting.")
        
    def _pose_cb(self, pose_stmp_msg):
        self.pose = pose_stmp_msg.pose

    def _get_marker(self, style='point', size=0.01, color='darkgreen'):
        """
        color can be any name from this page:
        http://matplotlib.org/mpl_examples/color/named_colors.hires.png
        """
        converter = colors.ColorConverter()
        c = converter.to_rgba(colors.cnames[color])
        m = Marker()
        m.header.frame_id = self.base_frame
        m.header.stamp = rospy.Time()
        m.id = 0
        if style == 'point':
            m.type = Marker.POINTS
        elif style == 'line':
            m.type = Marker.LINE_STRIP
        else:
            rospy.logerr("Unknown marker style: %s" % style)
            return None
        m.points = []
        m.action = Marker.MODIFY
        m.pose.orientation.w = 1.0
        m.scale.x = size
        m.color.r = c[0]
        m.color.g = c[1]
        m.color.b = c[2]
        m.color.a = c[3]
        return m

    
if __name__ == '__main__':
    rospy.init_node('position_trajectory_tracer')
    tv = PositionTrajectoryTracer()
    try:
        tv.run()
    except rospy.ROSInterruptException:
        pass
