#!/usr/bin/env python
import rospy
import actionlib
from ll4ma_trajectory_util.msg import JointTrajectoryAction, JointTrajectoryGoal


class JointTrajectoryActionClient:
    def __init__(self, rospy_init=True):
        if rospy_init:
            rospy.init_node("joint_trajectory_action_client")
        self.client = actionlib.SimpleActionClient("jtas",
                                                   JointTrajectoryAction)
        rospy.loginfo("Trying to connect with action server...")
        server_running = self.client.wait_for_server(
            timeout=rospy.Duration(10.0))
        if server_running:
            rospy.loginfo("Connected!")
        else:
            rospy.logwarn("You're probably connected to the action server.")
            # TODO make a better check if this is important, for some reason when
            # this is launched with Gazebo it says it times out waiting, but
            # does so immediately without actually waiting, even though it is in
            # fact connected. I think ROS is making more of a check than just
            # connection, so look into ROS source code for action server to check.

    def send_goal(self,
                  trajectory=None,
                  position=None,
                  wait_for_result=True,
                  timeout=30.0):
        rospy.loginfo("Sending goal to action server...")
        goal = JointTrajectoryGoal()
        if trajectory:
            goal.trajectory = trajectory
        elif position:
            goal.position = position
        self.client.send_goal(goal)
        rospy.loginfo("Goal sent successfully.")
        if wait_for_result:
            self._wait_for_result(timeout)
            return self.client.get_result().success
        else:
            return True

    def _wait_for_result(self, timeout=30.0):
        rospy.loginfo("Waiting for result...")
        success = self.client.wait_for_result(timeout=rospy.Duration(timeout))
        if success:
            rospy.loginfo("Result received!")
        else:
            rospy.logwarn("Timed out waiting for result.")


if __name__ == '__main__':
    # Test
    client = JointTrajectoryActionClient()
    client.send_goal()
