#!/usr/bin/env python
import rospy
import numpy as np
from copy import deepcopy
from scipy import interpolate
from actionlib import SimpleActionServer
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose, PoseStamped
from std_srvs.srv import SetBool, SetBoolResponse, Trigger, TriggerResponse
from ll4ma_trajectory_util.msg import (
    TaskTrajectoryAction, TaskTrajectoryFeedback, TaskTrajectoryResult)
from ll4ma_trajectory_msgs.srv import SetTaskTrajectory, SetTaskTrajectoryResponse
from ll4ma_trajectory_msgs.msg import TaskTrajectoryPoint
from ll4ma_logger_msgs.msg import RobotState
from rospy_service_helper import (zero_reflex_tactile, get_smooth_traj,
                                  set_controller_mode)
from pyquaternion import Quaternion as pyQuaternion
from ll4ma_movement_primitives.util import quaternion  # TODO quaternion should be in different pkg


class TaskTrajectoryActionServer:
    def __init__(self, pose_tolerance=0.01, goal_tolerance=0.01):
        self.pose_tolerance = pose_tolerance
        self.goal_tolerance = goal_tolerance
        self.rate_val = 500  # TODO get from server, and set appropriately
        self.timeout = 20.0  # TODO get from server
        self.rate = rospy.Rate(self.rate_val)
        self.duration = 6.0  # seconds, TODO get from server
        self.server = SimpleActionServer("ttas", TaskTrajectoryAction,
                                         self._goal_cb, False)
        self.feedback = TaskTrajectoryFeedback()
        self.result = TaskTrajectoryResult()
        self.robot_state = None

        # Services this node will call
        self.srvs = {
            "set_cmd_mode": "/lwr_controller_manager/enable_task_pd",
            "set_krc_mode": "/lwr_controller_manager/enable_krc_stiffness",
        }

        # Topics this node needs to know
        self.topics = {
            "task_cmd": "/lbr4/task_cmd",
            "robot_state": "/lbr4/robot_state",
            "viz_pose": "/lbr4/visualize_pose"
        }

        # Publishers
        self.viz_pose_pub = rospy.Publisher(
            self.topics["viz_pose"], PoseStamped, queue_size=1)
        self.task_cmd_pub = rospy.Publisher(
            self.topics["task_cmd"], PoseStamped, queue_size=1)

        # Subscribers
        rospy.Subscriber(self.topics["robot_state"], RobotState,
                         self._robot_state_cb)

        # Make sure all the services this node needs are up and running
        rospy.loginfo("Waiting for services...")
        for srv in self.srvs.keys():
            rospy.loginfo("    %s" % self.srvs[srv])
            rospy.wait_for_service(self.srvs[srv])
        rospy.loginfo("Services are up!")

    def start_server(self):
        rospy.loginfo("Server is running. Waiting for a goal...")
        self.server.start()
        while not rospy.is_shutdown():
            self.rate.sleep()

    def stop(self):
        rospy.loginfo("Server shutdown. Exiting.")

    def _goal_cb(self, action_goal):
        if action_goal.lift_height <= 0 or action_goal.lift_height >= 1.0:
            rospy.logerr("GOAL ABORTED. Lift height out of range")
            self.server.set_aborted()
            return False

        # Make sure we know where the real robot is
        if not self.robot_state or not self.robot_state.lbr4.joint_state.position:
            self.server.set_aborted()
            rospy.logerr("GOAL ABORTED. Current robot state unknown.")
            return False

        # Try to execute the lift
        rospy.loginfo("New action goal received.")
        self.result.success = False
        self._execute_lift(action_goal.lift_height)

        # Report result of execution to console and set status
        if self.result.success:
            rospy.loginfo("GOAL REACHED.")
            self.server.set_succeeded(self.result)
            return True
        else:
            rospy.logerr("FAILURE. Goal was not reached.")
            self.server.set_aborted()
            return False

    def _execute_lift(self, lift_height):
        start_point = TaskTrajectoryPoint()
        end_point = TaskTrajectoryPoint()
        current_pose = self.robot_state.end_effector_pose_base
        start_point.pose = deepcopy(current_pose)
        end_point.pose = deepcopy(current_pose)
        end_point.pose.position.z += lift_height
        interpolated_points = self._interpolate_points(start_point, end_point)

        start_pose = interpolated_points[0].pose
        goal_pose = interpolated_points[-1].pose

        # If robot is not at start then we have to bail
        current_pose = self.robot_state.end_effector_pose_base
        start_error = self._cartesian_error(current_pose, start_pose)
        at_start = start_error < self.pose_tolerance
        if not at_start:
            rospy.logerr("Robot is not at start position.")
            rospy.logerr("Error: {}".format(start_error))
            self.result.success = False
            return False

        rospy.loginfo("Executing trajectory...")
        # Set controller to command mode
        success = set_controller_mode(self.srvs["set_cmd_mode"])
        if not success:
            return False

        # Execute trajectory
        goal_converged = False
        timed_out = False
        start_time = rospy.get_time()
        cmd = PoseStamped()
        for point in interpolated_points:
            if goal_converged or timed_out:
                break

            # Send command to robot
            cmd.pose = point.pose
            cmd.header.stamp = rospy.Time.now()
            self.task_cmd_pub.publish(cmd)

            # Check convergence
            current_pose = self.robot_state.end_effector_pose_base
            goal_error = self._cartesian_error(current_pose, goal_pose)
            goal_converged = goal_error < self.goal_tolerance

            # Check timeout
            current_time = rospy.get_time() - start_time
            timed_out = current_time > self.timeout
            self.rate.sleep()

        # Give robot a moment to settle if not converged
        if not goal_converged:
            rospy.sleep(2.0)
        current_pose = self.robot_state.end_effector_pose_base
        goal_error = self._cartesian_error(current_pose, goal_pose)
        goal_converged = goal_error < self.goal_tolerance

        # Take controller out of command mode
        success = set_controller_mode(self.srvs["set_krc_mode"])

        rospy.loginfo("Execution complete.")

        # Report result and set status
        if goal_converged:
            rospy.loginfo("Converge to goal!")
            self.result.success = True
            return True
        elif timed_out:
            rospy.logwarn("Execution timed out!")
            self.result.success = False
            return False
        else:
            rospy.logwarn("Did not converge to goal.")
            self.result.success = False
            return False

    def _interpolate_points(self, start_point, end_point):
        # Interpolate position
        s = start_point.pose.position
        e = end_point.pose.position
        xs_f = interpolate.interp1d([0, self.duration], [s.x, e.x])
        ys_f = interpolate.interp1d([0, self.duration], [s.y, e.y])
        zs_f = interpolate.interp1d([0, self.duration], [s.z, e.z])
        ts = np.linspace(0.0, self.duration,
                         int(self.duration * self.rate_val))
        xs = xs_f(ts)
        ys = ys_f(ts)
        zs = zs_f(ts)

        # Interpolate orientation
        wp1 = start_point.pose.orientation
        wp2 = end_point.pose.orientation
        # We don't want to interpolate if they're already equal, will cause
        # divide by zero error in pyQuaternion
        wp1_arr = np.array([wp1.w, wp1.x, wp1.y, wp1.z])
        wp2_arr = np.array([wp2.w, wp2.x, wp2.y, wp2.z])
        if np.allclose(wp1_arr, wp2_arr):
            qs = [wp1_arr for _ in range(len(ts))]
        else:
            q1 = pyQuaternion(x=wp1.x, y=wp1.y, z=wp1.z, w=wp1.w)
            q2 = pyQuaternion(x=wp2.x, y=wp2.y, z=wp2.z, w=wp2.w)
            qs = pyQuaternion.intermediates(
                q1, q2, len(ts), include_endpoints=True)
            qs = [q.elements for q in qs]  # getting list form generator

        # Convert back to trajectory points
        interp_points = []
        for i in range(len(ts)):
            t_point = TaskTrajectoryPoint()
            t_point.pose.position.x = xs[i]
            t_point.pose.position.y = ys[i]
            t_point.pose.position.z = zs[i]
            t_point.pose.orientation.x = qs[i][1]
            t_point.pose.orientation.y = qs[i][2]
            t_point.pose.orientation.z = qs[i][3]
            t_point.pose.orientation.w = qs[i][0]
            interp_points.append(t_point)
        return interp_points

    def _cartesian_error(self, actual, desired, orientation=True, norm=True):
        p1 = actual.position
        p2 = desired.position
        p_err = np.array([p2.x - p1.x, p2.y - p1.y, p2.z - p1.z])
        if orientation:
            oa = actual.orientation
            od = desired.orientation
            q1 = np.array([oa.x, oa.y, oa.z, oa.w])
            q2 = np.array([od.x, od.y, od.z, od.w])
            q_err = np.squeeze(quaternion.err(q1, q2))
            pose_error = np.hstack((p_err, q_err))
        else:
            pose_error = np.hstack((p_err, np.zeros(3)))
        if norm:
            return np.linalg.norm(pose_error)
        else:
            return pose_error

    def _joint_error(self, actual, desired, norm=False):
        a = np.array(actual)
        d = np.array(desired)
        err = d - a
        if norm:
            err = np.linalg.norm(err)
        return err

    def _robot_state_cb(self, robot_state):
        if self.robot_state is None:
            rospy.loginfo("Robot state received!")
        self.robot_state = robot_state


if __name__ == '__main__':
    rospy.init_node("task_trajectory_action_server")
    try:
        server = TaskTrajectoryActionServer()
        rospy.on_shutdown(server.stop)
        server.start_server()
    except rospy.ROSInterruptException:
        pass
