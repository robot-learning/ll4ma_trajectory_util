#!/usr/bin/env python
import rospy
import actionlib
from ll4ma_trajectory_util.msg import TaskTrajectoryAction, TaskTrajectoryGoal


class TaskTrajectoryActionClient:
    def __init__(self, rospy_init=False):
        if rospy_init:
            rospy.init_node("task_trajectory_action_client")
        self.client = actionlib.SimpleActionClient("ttas",
                                                   TaskTrajectoryAction)
        rospy.loginfo("Trying to connect with action server...")
        server_running = self.client.wait_for_server(
            timeout=rospy.Duration(10.0))
        if server_running:
            rospy.loginfo("Connected!")
        else:
            rospy.logwarn("You're probably connected to the action server.")
            # TODO make a better check if this is important, for some reason
            # when this is launched with Gazebo it says it times out waiting,
            # but does so immediately without actually waiting, even though it
            # is in fact connected. I think ROS is making more of a check than
            # just connection, so look into ROS source code for action server
            # to check.

    def send_goal(self, lift_height, wait_for_result=True, timeout=30.0):
        rospy.loginfo("Sending goal to action server...")
        goal = TaskTrajectoryGoal(lift_height=lift_height)
        self.client.send_goal(goal)
        rospy.loginfo("Goal sent successfully.")
        if wait_for_result:
            self._wait_for_result(timeout)

    def _wait_for_result(self, timeout=30.0):
        rospy.loginfo("Waiting for result...")
        success = self.client.wait_for_result(timeout=rospy.Duration(timeout))
        if success:
            rospy.loginfo("Result received!")  # TODO check result?
        else:
            rospy.logwarn("Timed out waiting for result.")


if __name__ == '__main__':
    # Test
    client = TaskTrajectoryActionClient()
    client.send_goal(True)
