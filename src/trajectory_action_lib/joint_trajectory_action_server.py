#!/usr/bin/env python
import rospy
import numpy as np
from scipy import interpolate
from actionlib import SimpleActionServer
from sensor_msgs.msg import JointState
from std_srvs.srv import SetBool, SetBoolResponse, Trigger, TriggerResponse, TriggerRequest
from ll4ma_trajectory_util.msg import (
    JointTrajectoryAction, JointTrajectoryFeedback, JointTrajectoryResult)
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from ll4ma_logger_msgs.msg import RobotState
from rospy_service_helper import set_controller_mode, get_smooth_traj


class JointTrajectoryActionServer:
    def __init__(self, joint_tolerance=0.015, goal_tolerance=0.015):
        self.joint_tolerance = joint_tolerance
        self.goal_tolerance = goal_tolerance
        self.rate_val = 100  # TODO get from server, and set appropriately
        self.timeout = 20.0  # TODO get from server
        self.rate = rospy.Rate(self.rate_val)
        self.duration = 10.0  # seconds, TODO get from server
        self.time_steps = np.linspace(0.0, self.duration,
                                      int(self.duration * self.rate_val))
        self.server = SimpleActionServer("jtas", JointTrajectoryAction,
                                         self._goal_cb, False)
        self.feedback = JointTrajectoryFeedback()
        self.result = JointTrajectoryResult()
        self.robot_state = None
        self.jnt_names = []
        self.jnt_cmd = JointState()
        self._mts_trajectory = None
        self._start_position = []

        # Services this node will call
        self.srvs = {
            "set_cmd_mode": "/lwr_controller_manager/enable_joint_inv_dyn",
            "set_krc_mode": "/lwr_controller_manager/enable_krc_stiffness",
            "get_smooth_traj": "/get_smooth_trajectory",
        }

        # Topics this node needs to know
        self.topics = {
            "jnt_des_cmd": "/lbr4/joint_cmd",
            "robot_state": "/lbr4/robot_state",
        }

        # Publishers
        self.jnt_des_cmd_pub = rospy.Publisher(
            self.topics["jnt_des_cmd"], JointState, queue_size=1)
        # Subscribers
        rospy.Subscriber(self.topics["robot_state"], RobotState,
                         self._robot_state_cb)

        # Make sure all the services this node needs are up and running
        rospy.loginfo("Waiting for services...")
        for srv in self.srvs.keys():
            rospy.loginfo("    %s" % self.srvs[srv])
            rospy.wait_for_service(self.srvs[srv])
        rospy.loginfo("Services are up!")

    def start_server(self):
        rospy.loginfo("Server is running. Waiting for a goal...")
        self.server.start()
        while not rospy.is_shutdown():
            self.rate.sleep()

    def stop(self):
        rospy.loginfo("Server shutdown. Exiting.")

    def _goal_cb(self, action_goal):
        rospy.loginfo("New action goal received.")
        self.result.success = False

        # Make sure we know where the real robot is
        if self.robot_state is None or not self.robot_state.lbr4.joint_state.position:
            rospy.logwarn("Robot state unknown. Waiting...")
            i = 0
            while i < 100 and self.robot_state is None:
                rospy.sleep(0.1)
                i += 1
        if self.robot_state is None or not self.robot_state.lbr4.joint_state.position:
            self.server.set_aborted()
            rospy.logerr("GOAL ABORTED. Current robot state unknown.")
            return False

        if action_goal.trajectory.points:
            self._execute_trajectory(action_goal)
            # Report result of execution to console and set status
            if self.result.success:
                rospy.loginfo("GOAL REACHED.")
                self.server.set_succeeded(self.result)
            else:
                rospy.logwarn("FAILURE. Goal was not reached.")
                self.server.set_aborted()
        elif action_goal.position:
            self._execute_move_to_position(action_goal.position)
            if self.result.success:
                rospy.loginfo("START POSITION REACHED.")
                self.server.set_succeeded(self.result)
            else:
                rospy.logwarn("FAILURE. Start position was not reached.")
                self.server.set_aborted()
        else:
            self._execute_move_to_position(self._start_position)
            # Report result of execution to console and set status
            if self.result.success:
                rospy.loginfo("START POSITION REACHED.")
                self.server.set_succeeded(self.result)
            else:
                rospy.logwarn("FAILURE. Start position was not reached.")
                self.server.set_aborted()

    def _execute_move_to_position(self, position):
        if len(position) != len(self.jnt_names):
            rospy.logerr("Desired position length does not match state.")
            return False

        start_error = self._joint_error(
            self.robot_state.lbr4.joint_state.position, position)
        at_start = start_error < self.goal_tolerance
        if at_start:
            rospy.logwarn("Already at the start position!")
            return False

        self.jnt_cmd = JointState()
        self.jnt_cmd.name = self.jnt_names

        current_robot_point = JointTrajectoryPoint()
        current_robot_point.positions = self.robot_state.lbr4.joint_state.position
        goal_point = JointTrajectoryPoint()
        goal_point.positions = position
        # Interpolate linearly between the points
        joint_trajectory = JointTrajectory()
        joint_trajectory.points = self._interpolate_points(
            [current_robot_point, goal_point])
        if joint_trajectory.points:
            smooth_traj = get_smooth_traj(self.srvs["get_smooth_traj"],
                                          joint_trajectory)
            if not smooth_traj.points:
                rospy.logerr("Could not get smooth trajectory points.")
                return False
        else:
            rospy.logerr("Interpolation failed.")
            return False

        # Set controller to command mode
        if not set_controller_mode(self.srvs["set_cmd_mode"]):
            rospy.logerr("Could not put controller into COMMAND mode.")
            return False

        start_converged = False
        for point in smooth_traj.points:
            if start_converged:
                break
            # Execute the joint command for the point
            self._execute_point(point)
            # Check convergence
            start_error = self._joint_error(
                self.robot_state.lbr4.joint_state.position, position)
            start_converged = start_error < self.goal_tolerance
            self.rate.sleep()
        # Let robot settle for a moment if not converged
        if not start_converged:
            rospy.sleep(2.0)
        # Take controller out of command mode
        success = set_controller_mode(self.srvs["set_krc_mode"])
        if not success:
            rospy.logerr("Could not take controller out of COMMAND mode.")
            self.result.success = False
            return False
        if not start_converged:
            rospy.logerr("Did not converge to the start position.")
            self.result.success = False
            return False
        else:
            rospy.loginfo("Converged to start!")
            self.result.success = True
            return True

    def _execute_trajectory(self, action_goal):
        rospy.loginfo("Executing trajectory...")
        self.jnt_cmd = JointState()
        self.jnt_cmd.name = self.jnt_names

        start = action_goal.trajectory.points[0].positions
        goal = action_goal.trajectory.points[-1].positions

        # See if we're at start, if not then bail
        start_error = self._joint_error(
            self.robot_state.lbr4.joint_state.position, start)
        at_start = start_error < self.goal_tolerance
        if not at_start:
            rospy.logwarn("Not at start position:\n"
                          "    Error: {}\n"
                          "    Tolerance: {}".format(start_error,
                                                     self.goal_tolerance))
            return False

        # Set controller to command mode
        if not set_controller_mode(self.srvs["set_cmd_mode"]):
            return False

        # Execute trajectory points
        goal_converged = False
        timed_out = False
        start_time = rospy.get_time()
        for point in action_goal.trajectory.points:
            if goal_converged or timed_out:
                break
            # Execute joint command for point
            self._execute_point(point)
            # Check convergence
            goal_error = self._joint_error(
                self.robot_state.lbr4.joint_state.position, goal)
            goal_converged = goal_error < self.goal_tolerance
            # Check timeout
            current_time = rospy.get_time() - start_time
            timed_out = current_time > self.timeout
            self.rate.sleep()

        # Give robot a moment to settle if not converged
        if not goal_converged:
            rospy.sleep(2.0)
        goal_error = self._joint_error(
            self.robot_state.lbr4.joint_state.position, goal)
        goal_converged = goal_error < self.goal_tolerance

        # Take controller out of command mode
        set_controller_mode(self.srvs["set_krc_mode"])

        rospy.loginfo("Execution complete.")

        # Report result and set status
        if goal_converged:
            rospy.loginfo("Converge to goal!")
            self.result.success = True
            return True
        elif timed_out:
            rospy.logwarn("Execution timed out!")
            self.result.success = False
            return False
        else:
            rospy.logwarn("Did not converge to goal.")
            self.result.success = False
            return False

    def _execute_point(self, point):
        # Populate the joint command
        self.jnt_cmd.position = point.positions
        self.jnt_cmd.velocity = point.velocities
        self.jnt_cmd.effort = point.accelerations
        # Error check the command
        if (len(self.jnt_cmd.position) != self.num_jnts
                or len(self.jnt_cmd.velocity) != self.num_jnts
                or len(self.jnt_cmd.effort) != self.num_jnts
                or len(self.jnt_cmd.name) != self.num_jnts):
            rospy.logerr("Joint command does not match expected length.\n"
                         "    Num Joints: {}\n"
                         "     Name Size: {}\n"
                         "      Pos Size: {}\n"
                         "      Vel Size: {}\n"
                         "      Acc Size: {}\n".format(
                             self.num_jnts, len(self.jnt_names),
                             len(self.jnt_cmd.position),
                             len(self.jnt_cmd.velocity),
                             len(self.jnt_cmd.effort)))
            return False
        # Send joint command to robot
        self.jnt_cmd.header.stamp = rospy.Time.now()
        self.jnt_des_cmd_pub.publish(self.jnt_cmd)

    def _interpolate_points(self, raw_points):
        data = None
        for point in raw_points:
            p = np.array(point.positions)[:, None]
            data = p if data is None else np.hstack((data, p))
        interp_pos = None
        t = np.linspace(0.0, self.duration, data.shape[1])
        for i in range(data.shape[0]):
            f = interpolate.interp1d(t, data[i, :])
            dim_data = f(self.time_steps)
            interp_pos = dim_data if interp_pos is None else np.vstack(
                (interp_pos, dim_data))
        # Convert back to trajectory points
        interp_points = []
        for j in range(interp_pos.shape[1]):
            point = JointTrajectoryPoint()
            point.positions = interp_pos[:, j]
            # TODO these are just being populated to be valid command,
            # add if data is available
            point.velocities = np.zeros(len(point.positions))
            point.accelerations = np.zeros(len(point.positions))
            interp_points.append(point)
        return interp_points

    def _joint_error(self, actual, desired, norm=True):
        err = np.array(desired) - np.array(actual)
        if norm:
            err = np.linalg.norm(err)
        return err

    def _robot_state_cb(self, robot_state):
        if self.robot_state is None:
            self.num_jnts = len(robot_state.lbr4.joint_state.position)
            self.jnt_names = robot_state.lbr4.joint_state.name
            # Initialize the start point for move to start functionality
            self._start_position = robot_state.lbr4.joint_state.position
            rospy.loginfo("Robot state received:\n"
                          "    Num Joints: %d" % self.num_jnts)
        self.robot_state = robot_state


if __name__ == '__main__':
    rospy.init_node("joint_trajectory_action_server")
    import sys
    argv = rospy.myargv(argv=sys.argv)
    try:
        if len(argv) > 1:
            server = JointTrajectoryActionServer(*argv[1:])
        else:
            server = JointTrajectoryActionServer()
        rospy.on_shutdown(server.stop)
        server.start_server()
    except rospy.ROSInterruptException:
        pass
