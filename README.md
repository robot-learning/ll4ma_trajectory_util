**NOTE:** This package is very unstable right now as it has been used for different things at different times and is now just kind of a kitchen sink for various things. Need to make a pass on it to clean up and parse out the useful pieces and maybe even move them to other packages that make more sense.

# LL4MA Trajectory Utilities

This package offers various utilities for working with robot trajectories. Currently it primarily supports two capabilities: visualization of end-effector trajectories in rViz, and trajectory action servers for executing robot trajectories. 

For visualization, Marker display types are used to show the end-effector position (or any other task space position) as it traverses through space over time. It is configurable to choose the color and line style of the trace (solid or dotted). It also allows you to visualize many trajectories at once, e.g. if you have samples from a distribution of trajectories. Also offered is visualization of a full pose so that a Cartesian frame shows the pose over time, as well as a static trace of poses so that the entire pose trajectory can be seen at once.

The action servers/clients (joint and task space) are for commanding trajectories. These can be used to track the status of a trajectory being executed and publish interpolated waypoints to a low-level controller.


## TODO
I will add further documentation on usage when this package becomes more stable.
